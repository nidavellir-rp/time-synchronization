﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using static CitizenFX.Core.Native.API;

namespace TimeSync.Server
{
    public class TimeServer : BaseScript
    {
        private DateTime _baseTime;
        private HashSet<string> _driftingUsers = new HashSet<string>();

        /// <summary>
        /// Resource initialization
        /// </summary>
        /// <param name="resourceName"></param>
        [EventHandler("onResourceStart")]
        public async void OnResourceStart(string resourceName)
        {
            if (GetCurrentResourceName() != resourceName) return;

            // Get the server basetime 
            _baseTime = DateTime.UtcNow;

            // Allow the resource to start up for players
            await Delay(100);

            // Trigger new sync time to all connected players
            TriggerClientEvent("time-sync:sync-time", _baseTime.Ticks, DateTime.UtcNow.Ticks);
        }

        /// <summary>
        /// Send the synced time to the player
        /// </summary>
        /// <param name="player"></param>
        [EventHandler("time-sync:server:sync-time")]
        public void SyncTime([FromSource] Player player)
        {
            TriggerClientEvent("time-sync:sync-time", _baseTime.Ticks, DateTime.UtcNow.Ticks);
        }

        /// <summary>
        /// Receive reports of player's time drift
        /// </summary>
        /// <param name="player"></param>
        /// <param name="seconds"></param>
        [EventHandler("time-sync:server:drift-report")]
        public void OnDriftReport([FromSource] Player player, double seconds)
        {
            // Change the drift from seconds into timespan
            TimeSpan drift = TimeSpan.FromSeconds(seconds);

            // Log the player's time drift
            Debug.WriteLine($"{player.Name}'s system time is out of sync with server. Difference: {drift}");

            // Count the uders that have been added to drift
            _driftingUsers?.Add(player?.Identifiers.FirstOrDefault());

            // If multiple players are experiencing a drift, it may be a server issue
            if (_driftingUsers?.Count > 5)
            {
                Debug.WriteLine($"^8Number of users that have experienced excessive time drift: {_driftingUsers?.Count}\nIf a lot of users have this problem, the server clock may be out of sync!^7");
            }
        }
    }
}
