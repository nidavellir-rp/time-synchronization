﻿using CitizenFX.Core;
using System;
using static CitizenFX.Core.Native.API;

namespace TimeSync.Client
{
    public class TimeClient : BaseScript
    {
        private DateTime? _resourceStartTime;      // The start-up time of the server resource
        private double _systemDrift = 0;        // The user's system drift
        private double _gtaConversionRate = 30; // Default conversion rate is 30 seconds in-game passing every second in the real world
        private bool _reportTimeDrift = true;   // The client should report time drift to the server

        public TimeClient()
        {
            EventHandlers.Add("onClientResourceStart", new Action<string>(OnClientResourceStart));
            EventHandlers.Add("time-sync:sync-time", new Action<long, long>(OnTimeSync));
        }

        /// <summary>
        /// Resource initialization
        /// </summary>
        /// <param name="resourceName"></param>
        public void OnClientResourceStart(string resourceName)
        {
            if (GetCurrentResourceName() != resourceName) return;

            // As soon as possible we inform the server we want to sync our time
            TriggerServerEvent("time-sync:server:sync-time");
            SetTimeConversion();

            // Override from settings whether we should report users that have incorrect clock compared to the server
            if (bool.TryParse(GetResourceMetadata(resourceName, "report_player_time_deviation", 0), out bool reportTimeDrift))
            {
                _reportTimeDrift = reportTimeDrift;
            }
        }

        /// <summary>
        /// Figure out how many seconds need to pass in the real world before we 
        /// </summary>
        /// <param name="realWorldMinutes"></param>
        private void SetTimeConversion()
        {
            // Figure out if we're slowing down or speeding up time
            if (double.TryParse(GetResourceMetadata(GetCurrentResourceName(), "gta_hour_in_minutes", 0), out double realWorldMinutes))
            {
                // Ensure that we need to run per tick
                // If the default value (120 seconds = 1 GTA hour) is provided, we do not run a loop
                if (realWorldMinutes != 2)
                {
                    int millisecondsPerGameMinute = (int)Math.Round(TimeSpan.FromMinutes(realWorldMinutes).TotalMilliseconds * (1.0 / 60.0));

                    // Calculate the new difference
                    _gtaConversionRate = 60 / TimeSpan.FromMilliseconds(millisecondsPerGameMinute).TotalSeconds;

                    // Update the GTA time conversion rate
                    SetMillisecondsPerGameMinute(millisecondsPerGameMinute);
                }
            }
        }

        /// <summary>
        /// Synchronize the user's time with the server
        /// </summary>
        /// <param name="resourceStartTicks"></param>
        /// <param name="serverTimeTicks"></param>
        public void OnTimeSync(long resourceStartTicks, long serverTimeTicks)
        {
            // Get the current system UTC time
            DateTime systemUTC = DateTime.UtcNow;

            // Get the server's UTC time at the time of event triggering
            DateTime serverUTC = new DateTime(serverTimeTicks, DateTimeKind.Utc);

            // Set the resource start time
            _resourceStartTime = new DateTime(resourceStartTicks, DateTimeKind.Utc);

            // Work out the drift between server and system
            TimeSpan drift = serverUTC - systemUTC;

            // Round the drift to a whole second, and set it in variable
            _systemDrift = Math.Round(drift.TotalSeconds);

            // Check if we should report the drift, if reporting is enabled and drift exceeds 2 minutes
            if (_reportTimeDrift && (_systemDrift <= -120 || 120 <= _systemDrift))
            {
                // Time difference is more than a minute, even factoring in latency that is a lot
                TriggerServerEvent("time-sync:server:drift-report", _systemDrift);
            }

            // Set the world clock for the user
            OverrideNetworkClockTime();
        }

        /// <summary>
        /// Convert UTC time to GTA time, account for system time drift
        /// </summary>
        /// <returns></returns>
        private TimeSpan GetConvertedTime()
        {
            // Ensure that the resource start time has been set
            if (!_resourceStartTime.HasValue) return default;

            // Work out the elapsed time since resource started on server
            TimeSpan elapsed = DateTime.UtcNow
                                    // Account for the system drift
                                    .AddSeconds(_systemDrift)
                                    // Work out the time since the server resource started
                                    .Subtract(_resourceStartTime.Value);

            // Work out the GTA time based on elapsed time
            return _resourceStartTime?
                    // Add how much time has passed in GTA
                    .AddSeconds(elapsed.TotalSeconds * _gtaConversionRate)
                    // Get the time of day
                    .TimeOfDay ?? default;
        }

        /// <summary>
        /// Gets the converted GTA time and sets the client clock to match
        /// </summary>
        public void OverrideNetworkClockTime()
        {
            if (!_resourceStartTime.HasValue) return;

            // Get the converted GTA time
            TimeSpan gtaDateTime = GetConvertedTime();

            // Update the client clock
            NetworkOverrideClockTime(gtaDateTime.Hours, gtaDateTime.Minutes, gtaDateTime.Seconds);
            SetTimeConversion();
        }
    }
}
