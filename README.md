# Time Synchronization

This resource helps you keep all the players on your server in the same time cycle.

You will also be able to extend or shorten the day on your server, while keeping everyone in sync.

### Configuration

#### gta_hour_in_minutes '2'
*Type:* double  
*Default value:* 2

By default the time in GTA is 1 hour in game every 2 minutes. Enabling this option and changing its value will affect how fast time passes in the game.

#### report_player_time_deviation 'true'
*Type:* boolean  
*Default value:* true

Report to the server console when players with out of sync computers connect to your server / sync with the server time.