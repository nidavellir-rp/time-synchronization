-- Resource Metadata
fx_version 'adamant'
games {'gta5'}

author 'Nidavellir RP'
description 'Time synchronization resource, that allows for extending or shortening days to the server preferences'
version 'custom-build'

-- !!Optional settings!!

-- Make the GTA days longer or shorter, GTA Online runs at 1 in-game hour every 2 real minutes
-- Default value: 2
-- gta_hour_in_minutes '2'

-- Log to server console the name of users and how much their time deviates from the server
-- This option is enabled by default
-- report_player_time_deviation 'true'

client_script 'client.net.dll'
server_script 'server.net.dll'